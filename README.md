# autosig-sample-watchdog

This package contains some simple tools to interface with the watchdog
that is implemented in the `runvm --watchdog` feature in the automotive sig.

It also has systemd services that allows (if enabled) integrating the
watchdog with an ostree update. With this, a watchdog is started
before deploying the update and then stopped on a successful boot,
otherwise reseting and causing a roll-back.

